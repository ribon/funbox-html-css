// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

import React from 'react';

function Picture({ image, alt }) {
  return (
    <picture>
      <source
        type="image/webp"
        srcSet={`/images/${image}@1x.webp 1x, /images/${image}@2x.webp 2x`}
      />

      <img
        className="product__image"
        src={`/images/${image}@1x.png`}
        srcSet={`/images/${image}@2x.png 2x`}
        alt={alt}
      />
    </picture>
  );
}

Picture.propTypes = {
  image: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired
};

export default Picture;
