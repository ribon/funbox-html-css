// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

import React, { Component } from 'react';

import Picture from 'components/picture';
import Feauters from 'components/feauters';
import TextStatus from 'components/text-status';
import './product.scss';

export default class Product extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    features: PropTypes.arrayOf(PropTypes.string).isRequired,
    image: PropTypes.string.isRequired,
    weight: PropTypes.objectOf(PropTypes.string).isRequired,
    composition: PropTypes.string.isRequired,
    disabled: PropTypes.bool.isRequired,
    selected: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired
  };

  state = { isHover: false };

  onMouseLeave = () => {
    if (!this.props.selected) return; // eslint-disable-line react/destructuring-assignment

    this.setState({ isHover: true });
  };

  onMouseEnter = () => {
    if (!this.props.selected) return; // eslint-disable-line react/destructuring-assignment

    this.setState({ isHover: false });
  };

  render() {
    const {
      title,
      subtitle,
      weight,
      disabled,
      selected,
      onSelect,
      image,
      features,
      composition
    } = this.props;

    const { isHover } = this.state;

    let productClass = 'products__item product';
    let textStatus = <TextStatus onSelect={onSelect} />;
    let textTeaser = (
      <p className="product__teaser">Сказочное заморское яство</p>
    );

    if (selected) {
      productClass += ' product--selected';
      textStatus = <TextStatus selectedText={composition} />;
    }

    if (isHover) {
      productClass += ' product--selected-hover';
      textTeaser = (
        <p className="product__teaser product__teaser--red">
          Котэ не одобряет?
        </p>
      );
    }

    if (disabled) {
      productClass += ' product--disabled';
      textStatus = <TextStatus disabledText={subtitle} />;
    }

    return (
      <div className={productClass}>
        <button
          className="product__wrapper"
          type="button"
          onClick={onSelect}
          onMouseLeave={this.onMouseLeave}
          onMouseEnter={this.onMouseEnter}
        >
          <div className="product__description">
            {textTeaser}

            <h3 className="product__title">
              {title}
              <span className="product__subtitle">{subtitle}</span>
            </h3>

            <Feauters features={features} />
          </div>

          <Picture image={image} alt={title + subtitle} />

          <div className="product__weight">
            <span className="product__weight-amount">{weight.amount}</span>
            <span className="product__unit">{weight.unit}</span>
          </div>
        </button>

        {textStatus}
      </div>
    );
  }
}
