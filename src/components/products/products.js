import React, { Component } from 'react';

import Product from 'components/product';
import data from 'data/data';

import './producs.scss';

export default class Products extends Component {
  state = {
    products: data
  };

  onProductClick = id => {
    this.toggleProperty(id, 'selected');
  };

  toggleProperty(id, property) {
    this.setState(({ products }) => {
      const index = products.findIndex(product => product.id === id);
      const newValue = { [property]: !products[index][property] };

      const newState = [
        ...products.slice(0, index),
        { ...products[index], ...newValue },
        ...products.slice(index + 1)
      ];

      return { products: newState };
    });
  }

  render() {
    const { products } = this.state;
    const productsElements = products.map(product => {
      return (
        <Product
          key={product.id}
          {...product}
          onSelect={evt => {
            evt.preventDefault();
            this.onProductClick(product.id);
          }}
        />
      );
    });

    return <div className="products">{productsElements}</div>;
  }
}
