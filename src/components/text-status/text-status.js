import React from 'react';

export default function({
  disabledText = false,
  selectedText = false,
  onSelect = false
}) {
  let textStatus = (
    <p className="product__text-status">
      Чего сидишь? Порадуй котэ,&nbsp;
      <button onClick={onSelect} className="product__btn-buy" type="button">
        купи
      </button>
      <span>.</span>
    </p>
  );

  if (selectedText) {
    textStatus = <p className="product__text-status">{selectedText}</p>;
  }

  if (disabledText) {
    textStatus = (
      <p className="product__text-status">
        Печалька, {disabledText} закончился.
      </p>
    );
  }

  return textStatus;
}
