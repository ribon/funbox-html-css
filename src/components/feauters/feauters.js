// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

import React from 'react';

function Feauters({ features }) {
  const feautersList = features.map((feature, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <li key={index} dangerouslySetInnerHTML={{ __html: feature }} />
  ));

  return <ul className="product__features">{feautersList}</ul>;
}

Feauters.propTypes = {
  features: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default Feauters;
