import React from 'react';
import Products from 'components/products';

export default function() {
  return (
    <div className="wrapper">
      <main className="main">
        <h1 className="title">Ты сегодня покормил кота?</h1>

        <Products />
      </main>
    </div>
  );
}
