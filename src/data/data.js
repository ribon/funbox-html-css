export default [
  {
    id: '19730183-dadb-45d4-b8ea-cc7a3a63202d',
    title: 'Нямушка',
    subtitle: 'с фуа-гра',
    features: ['<b>10</b> порций', 'мышь в подарок'],
    image: 'prototype_1',
    weight: {
      amount: '0,5',
      unit: 'кг'
    },
    composition: 'Печень утки разварная с артишоками.',
    selected: false,
    disabled: false
  },
  {
    id: '35a0b9cc-a1c9-49a8-85ee-45973eb110ad',
    title: 'Нямушка',
    subtitle: 'с рыбой',
    features: ['<b>40</b> порций', '<b>2</b> мыши в подарок'],
    image: 'prototype_1',
    weight: {
      amount: '2',
      unit: 'кг'
    },
    composition: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
    selected: true,
    disabled: false
  },
  {
    id: '3a17c07c-2f88-4ec2-8fd0-7082a225765e',
    title: 'Нямушка',
    subtitle: 'с курой',
    features: [
      '<b>40</b> порций',
      '<b>5</b> мышей в подарок',
      'заказчик доволен'
    ],
    image: 'prototype_1',
    weight: {
      amount: '5',
      unit: 'кг'
    },
    composition: 'Филе из цыплят с трюфелями в бульоне.',
    selected: false,
    disabled: true
  }
];
